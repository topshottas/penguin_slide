using UnityEngine;
using System.Collections;

public class platform_spawner : MonoBehaviour {
	private float pingu_max_jump_x = 6f;					//determine through trial and error; will need to be changed if vertical or horizontal velocities are changed

	private float size_of_standing_block;					
	private float size_of_platform;
	private float size_of_pingu;
	private float background_size ;

	private float position_last_platform;

	public GameObject platform_prefab;
	private GameObject[] platform_game_object;  			

	private float distance_min;
	private float distance_max;
	private float spawn_pos;

	//adam's freaky parameters
	private int num_platforms;
	private int platform_index;
	private int total_platforms = 5;
	//it is obvious that naveen is just a freaky variable hater

	void Start () {
		platform_game_object = new GameObject[total_platforms];
		platform_index = 4;
		background_size = GameObject.FindGameObjectWithTag("Background").GetComponent<SpriteRenderer>().bounds.size.x;
		//Creation of the first platform and assigning id = 1 to it
		size_of_standing_block = GameObject.FindWithTag ("Standing Block").GetComponent<SpriteRenderer>().bounds.size.x;			//get size of the standing block - ensure standing block x position is 0
		size_of_pingu = GameObject.Find ("Graphics").GetComponent<SpriteRenderer> ().bounds.size.x;					//get size of pingu  
		//print (size_of_pingu);
		distance_min = (size_of_standing_block/2f) + (size_of_pingu*.5f);															//min distance for first block will be half the size of standing block (this will give the x position where standing block ends -assuming at x = 0) + size of pingu(with 1.25 saftey factor)
		distance_max = size_of_standing_block/2f + pingu_max_jump_x;																//max distance is size_of_standing_block + pingu's max x jump distance
		spawn_pos = Random.Range (distance_min, distance_max);																		
		//print (distance_min);
		///print (distance_max);
		platform_game_object[0] = Instantiate (platform_prefab, new Vector3 (spawn_pos, 0, 0), Quaternion.identity) as GameObject;	
		platform_game_object[0].GetComponent<platforms>().id = 1;																	//assign id=1 to first platform
		
		//Creation of additional 4 platforms
		size_of_platform = platform_game_object [0].GetComponent<SpriteRenderer> ().bounds.size.x;									//getting size of platforms

		for (int i = 0; i < total_platforms-1; i++) {
			position_last_platform = platform_game_object [i].transform.position.x;														//getting poistion of first platform
			distance_min = position_last_platform + (size_of_platform * .5f) + (size_of_pingu * .5f);
			distance_max = position_last_platform + pingu_max_jump_x;
			spawn_pos = Random.Range (distance_min, distance_max);
			platform_game_object[i+1] = Instantiate (platform_prefab, new Vector3 (spawn_pos, 0, 0), Quaternion.identity) as GameObject;
			platform_game_object[i+1].GetComponent<platforms>().id = i+2;;
		}
	}
	
	// Update is called once per frame
	void Update () {
		float max_pos;
		int next_index = (platform_index % 4) + 1;
		num_platforms = GameObject.FindGameObjectsWithTag ("Platform").Length; 

		if (num_platforms <= 5) {
			total_platforms++;
			int platforms_spawned = PlayerPrefs.GetInt ("platforms");
			PlayerPrefs.SetInt ("platforms", platforms_spawned + 1);
			position_last_platform = platform_game_object[platform_index].transform.position.x;														//getting poistion of first platform
			max_pos = Mathf.Max(position_last_platform + size_of_platform/2f, background_size);
			distance_min =  max_pos + size_of_pingu*.5f + size_of_platform/2f;
			distance_max = max_pos + pingu_max_jump_x;
			spawn_pos = Random.Range (distance_min, distance_max);			
			platform_game_object[next_index] = Instantiate(platform_prefab,new Vector3(spawn_pos,0,0),Quaternion.identity) as GameObject;
			platform_game_object[next_index].GetComponent<platforms>().id = total_platforms;
			platform_index = next_index;
		};
	}

}                                                                                     