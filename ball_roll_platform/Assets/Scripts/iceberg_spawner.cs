﻿using UnityEngine;
using System.Collections;

public class iceberg_spawner : MonoBehaviour {
	private Camera cam;
	private float camera_height;
	private float camera_width;
	
	private float random_height;				//y-value for iceberg placement
	private float random_width = 0;				//x-value for iceberg placement
	
	private int num_of_icebergs;			
	private int which_iceberg;
	private float iceberg_width;
	public GameObject[] icebergs_prefab;
	private GameObject icebergs_game_object;
	
	void Awake(){
		
	}
	// Use this for initialization
	void Start () {
		cam = Camera.main;
		camera_height = 2f*cam.GetComponent<camera_position> ().height;
		camera_width = 2f*cam.GetComponent<camera_position> ().width;
		num_of_icebergs = 1;
		Spawn_iceberg (true);
	}
	
	// Update is called once per frame
	void Update () {
		num_of_icebergs = GameObject.FindGameObjectsWithTag ("IceBergs").Length;
		//Spawn more icebergs if less than...
		if (num_of_icebergs < 2) {
			Spawn_iceberg(false);			//second value '0' only needed for initial icebergs
		}
	}
	
	void Spawn_iceberg (bool initial) {
		which_iceberg = (int)(Random.Range (0, icebergs_prefab.Length));
		random_height = Random.Range (.3f * camera_height, .45f * camera_height);
		if (initial) {
			random_width = Random.Range (0, camera_width);
		} else {
			random_width = Random.Range (cam.transform.position.x + camera_width, cam.transform.position.x + camera_width * 2f);
		}
		icebergs_game_object = Instantiate (icebergs_prefab[which_iceberg], new Vector3 (random_width, random_height, 0), Quaternion.identity) as GameObject;
	}
}
