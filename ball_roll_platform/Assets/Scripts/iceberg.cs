﻿using UnityEngine;
using System.Collections;

public class iceberg : MonoBehaviour {
	private float cam_start_pos;
	private float iceberg_speed;
	private float iceberg_size;
	private float background_size;
	
	void Awake(){
		transform.parent = Camera.main.transform;
	}
	// Use this for initialization 	
	void Start () { 	
		iceberg_speed = Random.Range (0.01f, 0.03f);						//allow icebergs to float by at variable speeds
		cam_start_pos = Camera.main.gameObject.transform.position.x;
		background_size = GameObject.Find("Background").GetComponent<SpriteRenderer> ().bounds.size.x;
	} 	 

	// Update is called once per frame 	
	void FixedUpdate () {
		//move iceberg left by iceberg speed
		transform.position = transform.position + new Vector3 (-iceberg_speed, 0, 0);
		//Destroy iceberg after it passes the kill position
		if (transform.position.x < (Camera.main.transform.position.x - background_size)){ 
			Destroy(gameObject);
		}
	}
}
