﻿using UnityEngine;
using System.Collections;

public class clouds : MonoBehaviour {
	private float cam_start_pos;
	private float cloud_speed;
	private float cloud_size;
	private float background_size;

	void Awake(){
		transform.parent = Camera.main.transform;
	}
	// Use this for initialization 	
	void Start () { 	
		cloud_speed = Random.Range (0.01f, 0.03f);						//allow clouds to float by at variable speeds
		cam_start_pos = Camera.main.gameObject.transform.position.x;
		background_size = GameObject.Find("Background").GetComponent<SpriteRenderer> ().bounds.size.x;
	} 	 	

	// Update is called once per frame 	
	void FixedUpdate () {
		//move cloud left by cloud speed
		transform.position = transform.position + new Vector3 (-cloud_speed, 0, 0);
		//Destroy cloud after it passes the kill position
		if (transform.position.x < (Camera.main.transform.position.x - (background_size*.67f)) ){ 
			Destroy(gameObject);
		}

	}
}
