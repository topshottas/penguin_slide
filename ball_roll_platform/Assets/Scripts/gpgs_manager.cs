using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.Android;

/* This script manages the signing in/out of the user 
as well as handles all of the achievments and leaderboards.
To achieve seamless integration it was crucial to compartmentalize this
portion of our code and have it run at a high level between scenes. */


public class gpgs_manager : MonoBehaviour
{
	public bool signedin;
	//public static GameObject signin_button_text; 
	//public static GameObject signin_button;

	//public UnityEngine.Events.UnityAction push_signin;
	public UnityEngine.Events.UnityAction push_achievements;
	public UnityEngine.Events.UnityAction push_leaderboard;


	//These need to remain here
	void Awake() {
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate ();
	}
	
	// Use this for initialization
	void Start ()
	{
		DontDestroyOnLoad (GameObject.Find ("GPGS_manager"));
		int previously_in = PlayerPrefs.GetInt ("signedin");

		signedin = false;

		if (previously_in == 1) {
			if (! PlayGamesPlatform.Instance.localUser.authenticated) {
				PlayGamesPlatform.Instance.Authenticate ((bool success) => {
					if (success) {
						// Signed in! Hooray!
						//signin_button_text.GetComponent<Text>().text = "Sign-Out From Google Services";
						signedin = true;
						GameObject.Find ("EventSystem").GetComponent<manager> ().CompletedSignIn (1);
					} else {
						// Not signed in. We'll want to show a sign in button
						//signin_button_text.GetComponent<Text>().text = "Sign-In to Google Services";
						signedin = false;
						GameObject.Find ("EventSystem").GetComponent<manager> ().CompletedSignIn (0);
					}
				}, true);   // <--- That "true" is very important!
			} else {
				signedin = true;
				Debug.Log ("We're already signed in");
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		//if (Application.loadedLevel == 1) {


			//if (signedin) {
				//signin_button_text.GetComponent<Text>().text = "Sign-Out From Google Services";
		//	}
	//	}
	}


	// Called when the player select to log in to Google Play
	//returns a 1 if user is signed in, returns a 0 if user is signed out
	public void OpenSignIn() {
		if (signedin == false) {
			Social.localUser.Authenticate (success => {
				
				if (success) {
					signedin = true;
					Debug.Log ("Authentication successful");
					string userInfo = "Username: " + Social.localUser.userName + 
						"\nUser ID: " + Social.localUser.id + 
							"\nIsUnderage: " + Social.localUser.underage;
					Debug.Log (userInfo);
					GameObject.Find("EventSystem").GetComponent<manager>().CompletedSignIn(1);
				} else {
					Debug.Log ("Authentication failed");
					signedin = false;
					GameObject.Find("EventSystem").GetComponent<manager>().CompletedSignIn(0);
				}
				//signin_info.GetComponent<Text>().text = status_info;
			});
		} else {
			PlayGamesPlatform.Instance.SignOut();
			//signin_button_text.GetComponent<Text>().text = "Sign-In to Google Services";
			signedin = false;
			GameObject.Find("EventSystem").GetComponent<manager>().CompletedSignIn(2);
			//	signin_info.GetComponent<Text>().text = status_info;
		}
	}

	public int OpenAchievements() {
		if (signedin) {
			Social.ShowAchievementsUI ();
			return 1;
		} else {
			return 0;
		}
	}

	public int OpenLeaderboard() {
		if (signedin) {
			Social.ShowLeaderboardUI();
			return 1;
		} else {
			return 0;
		}
	}

	// returns the player highscore
	public void ReportScore(int currentscore) {
		// post score 12345 to leaderboard ID "CgkI0fvDo4ANEAIQAQ"

		if (!signedin) {
			return;
		}

		Social.ReportScore ((long)currentscore, "CgkI0fvDo4ANEAIQAQ", (bool success) => {
			// handle success or failure
		});

	}

	public void AchievementCheck(int currentscore, int highscore, int deaths, int edges, int total, int platforms) {

		if (!signedin) {
			return;
		}

		int onetwothree = PlayerPrefs.GetInt ("onetwothree");

		switch (onetwothree) {
		case 0:
			if (currentscore == 1) {
				PlayerPrefs.SetInt ("onetwothree", 1);
			}
			break;
		case 1:
			if (currentscore == 2) {
				PlayerPrefs.SetInt ("onetwothree", 2);
			}
			break;
		case 2:
			if (currentscore == 3) {
				PlayerPrefs.SetInt ("onetwothree", 3);
				Social.ReportProgress ("CgkI0fvDo4ANEAIQFg", 100.0f, (bool success) => {
					// handle success or failure
				});
			}
			break;
		default:
			break;
		}
			

		// HIGHSCORE ACHIEVES
		if ((currentscore >= 10) && (highscore < 10)) { 					// Broke The Ice achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQAg", 100.0f, null);
		}
		if ((currentscore >= 25) && (highscore < 25)) { 					//Everydayer achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQAw", 100.0f, null);
		}
		if ((currentscore >= 50) && (highscore < 50)) { 					//Skater achievement
			Social.ReportProgress("CgkI0fvDo4ANEAIQBA", 100.0f, null);
		}
		if ((currentscore >= 75) && (highscore < 75)) { 					//Beats Curling achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQBQ", 100.0f, null);
		}
		if ((currentscore >= 100) && (highscore < 100)) { 					//Ice Ninja achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQBw", 100.0f, null);
		}
		if ((currentscore >= 150) && (highscore < 150)) { 					//Ice Master achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQCA", 100.0f, null);
		}
		if ((currentscore >= 200) && (highscore < 200)) { 					//The Penguin achievement	
			Social.ReportProgress ("CgkI0fvDo4ANEAIQBg", 100.0f, null);
		}
		if ((currentscore >= 250) && (highscore < 200)) { 											//Emperor Penguin achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQCQ", 100.0f, null);
		}

		// DEATHCOUNT ACHIEVES
		if (deaths == 1) { 												// First One Comes Quick achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQCg", 100.0f, null);
		}
		if (deaths == 50) { 												// Slippery When Wet achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQCw", 100.0f, null);
		}
		if (deaths == 100) { 												// Face Full of Icy Water achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQDA", 100.0f, null);
		}
		if (deaths == 250) { 												// Frosty! achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQDQ", 100.0f, null);
		}
		if (deaths == 500) { 												// Ice Bucket Challenge achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQDg", 100.0f, null);
		}
		if (deaths == 1000) { 												// All Surf No Turf achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQDw", 100.0f, null);
		}
		if (deaths == 2500) { 												// That's Commitment! achievement
			Social.ReportProgress ("CgkI0fvDo4ANEAIQEA", 100.0f, null);
		}

		// PLATFORM SPAWNING ACHIEVES		
		if ( (platforms >= 1000) && (platforms < 2500)) {					// A Handful achievment
			Social.ReportProgress("CgkI0fvDo4ANEAIQEg", 100.0f, null);
		}
		if ( (platforms >= 2500) && (platforms < 5000)) {					// Where Do They Come From achievment
			Social.ReportProgress("CgkI0fvDo4ANEAIQEg", 100.0f, null);
		}
		if ( (platforms >= 5000) && (platforms < 10000)) {					// Missed opportunities achievment
			Social.ReportProgress("CgkI0fvDo4ANEAIQEg", 100.0f, null);
		}
		if ( platforms >= 10000 ) {											// Platform Killer achievment
			Social.ReportProgress("CgkI0fvDo4ANEAIQEg", 100.0f, null);
		}
		

		// Edge Clipping Achieves
		if (edges <= 200) {
			if (edges == 10) {													// Edge Clipper achievement
				Social.ReportProgress ("CgkI0fvDo4ANEAIQGA", 100.0f, null);
			}
			if (edges == 50) {													// Rough Edges achievement
				Social.ReportProgress ("CgkI0fvDo4ANEAIQGQ", 100.0f, null);
			}
			if (edges == 100) {													// Slippery Edges achievement
				Social.ReportProgress ("CgkI0fvDo4ANEAIQGg", 100.0f, null);
			}
			if (edges == 150) {													// The Cutting Edge achievement
				Social.ReportProgress ("CgkI0fvDo4ANEAIQGw", 100.0f, null);
			}
			if (edges == 200) {													// The Bleeding Edge achievement
				Social.ReportProgress ("CgkI0fvDo4ANEAIQHA", 100.0f, null);
			}
		}

		//Total Achieves
		if (total < 100001) {
			//float result = total/1000;	
			Social.ReportProgress ("CgkI0fvDo4ANEAIQHQ", total/10.0f, null);	// Novice Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQHg", total/50.0f, null);		// Competent Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQHg", total/50.0f, null);			// Competent Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQHw", total/100.0f, null);		// Talented Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQIA", total/250.0f, null);			// Apprentice Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQIQ", total/489.73f, null);			// Seasoned Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQIg", total/750.0f, null);	// Journeyman Slider achievement

			Social.ReportProgress ("CgkI0fvDo4ANEAIQIw", total/1000.01f, null);  	// Master Slider
		}
	}



}

