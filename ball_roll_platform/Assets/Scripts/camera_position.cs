﻿using UnityEngine;
using System.Collections;

public class camera_position : MonoBehaviour {
	public Camera cam;
	public float height;
	public float width;
	public GameObject pingu;
	public float x_from_pingu;
	public Vector3 start_cam_pos;

	// Use this for initialization
	void Awake() {
		cam = Camera.main;
		height = cam.orthographicSize;
		width = height * cam.aspect;
		start_cam_pos = cam.transform.position  + new Vector3 (width, height, -1);
		cam.transform.position = start_cam_pos;
	}

	void Start () { 
		if (pingu) {
			x_from_pingu = cam.transform.position.x - pingu.transform.position.x;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (pingu) {
			transform.position = new Vector3 (pingu.transform.position.x + x_from_pingu, transform.position.y, transform.position.z);
		}
	}
}
