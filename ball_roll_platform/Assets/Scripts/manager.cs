﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class manager : MonoBehaviour {

	private GameObject pingu_go;
	private GameObject score_go;			//canvas object which displays the score
	private GameObject points_go;			//canvas object which displays gained points
	private GameObject splash_go;

	private GameObject score_image_go;
	private GameObject overlay_go;
	private GameObject final_score_go;
	private GameObject final_best_go;
	private Sprite[] ui_sprite;
	private Image sound_image;
	private Image music_image;

	//Pause variables
	public bool paused;					    //is the game paused 
	private GameObject pause_text_go;
	private GameObject pause_go;

	//Tutorial variables
	private Image tutorial_image;			//Image object in the UI displaying the sprite
	private Sprite[] tutorial_sprite;		//the sprite pulled from resources folders
	private int tutorial_page;			    //which tutorial "page" is currently being displayed
	private GameObject previous;			//button for next tutorial page
	private GameObject next;				//button for previous tutorial page
	private GameObject prefab;
	private GameObject[] platform_game_object; 
	private GameObject tap;

	//Scoring variables
	private int score;
	private float color_alpha;				//the alpha code for the color (rgba) of the points
	private int last_platform;				//ID of the last platform that pingu collided with
	private int new_platform;				//ID of the new platform that pingu collided with

	//Audio variables
	private GameObject sound;					 	//are sound effects on/off
	private AudioSource source;

	//Other variables
	private bool info_visible;
	private GameObject info_panel;
	private GameObject gpgs_m;


	//Sign In/Out Variables
	private bool signedin;
	private bool signin_visible;
	private GameObject signin_panel;
	private GameObject signin_info;
	private GameObject signin_button;
	private GameObject signin_button_text;  

	//Initialize based on which level was just loaded
	void Start() {
		switch (Application.loadedLevel) {				
		case 0:											//SPLASH
			splash_go = GameObject.Find ("Splash");
			StartCoroutine ("DisplaySplashFade");
			break;
		case 1:											//MAIN Menu

			info_visible = false;
			info_panel = GameObject.Find("Info_panel");
			info_panel.SetActive(false);

			signin_button_text = GameObject.Find("Login_text");
			signin_button = GameObject.Find ("Login");

			gpgs_m = GameObject.Find ("GPGS_manager");
			if (gpgs_m.GetComponent<gpgs_manager> ().signedin) {
				signin_button_text.GetComponent<Text> ().text = "Sign out of Google Services";
			} else {
				signin_button_text.GetComponent<Text> ().text = "Sign in to Google Services";
			}
			signin_visible = false;
			signin_panel = GameObject.Find("SignIn_panel");
			signin_info = GameObject.Find("Info_text");
			signin_panel.SetActive(false);

			sound = GameObject.Find("Song");
			ui_sprite = Resources.LoadAll<Sprite>("UI_changable");
			source = GameObject.Find("Song").GetComponent<AudioSource>();
			sound_image = GameObject.Find("Sound_image").GetComponent<Image>();
			music_image = GameObject.Find("Music_image").GetComponent<Image>();


			//if (source.isPlaying) {
			if (Convert.ToBoolean(PlayerPrefs.GetInt("music"))) {
				music_image.sprite = ui_sprite[1];
				if (source.isPlaying == false) {
					source.Play();
				}
			} else {
				music_image.sprite = ui_sprite[0];
			}

			//if (sound.GetComponent<Song>().fx) {
			if (Convert.ToBoolean(PlayerPrefs.GetInt("fx"))) {
				sound_image.sprite = ui_sprite[3];
			} else {
				sound_image.sprite = ui_sprite[2];
			}

			Time.timeScale = 1;
			break;
		case 2:											//GAME SCENE
			gpgs_m = GameObject.Find ("GPGS_manager");
			paused = false;
			pause_text_go = GameObject.Find ("PausedText");
			Time.timeScale = 1;
			overlay_go = GameObject.Find("Overlay");
			overlay_go.SetActive(false);
			pause_go = GameObject.Find ("PauseButton");
			score_image_go = GameObject.Find("ScoreImage");
			score = 0;
			last_platform = 0;
			pingu_go = GameObject.FindWithTag ("Pingu");
			score_go = GameObject.FindWithTag ("Score");
			points_go = GameObject.FindWithTag ("Points");
			color_alpha = 255f;
			break;
		case 3:											//Tutorial
			pingu_go = GameObject.FindWithTag ("Pingu");
			score_image_go = GameObject.Find("ScoreImage");
			score_go = GameObject.FindWithTag ("Score");
			tutorial_image = GameObject.Find ("Tutorial_text").GetComponent<Image>();
			tutorial_page = 0;
			tutorial_sprite = Resources.LoadAll<Sprite>("Tutorial_Text");
			tap = GameObject.Find("Tap");
			tap.SetActive(false);
			previous = GameObject.Find ("Previous");
			next = GameObject.Find ("Next");
			previous.SetActive(false);
			tutorial_image.sprite = tutorial_sprite [tutorial_page];
			platform_game_object = new GameObject[7];
			prefab = Camera.main.GetComponent<platform_spawner>().platform_prefab;
			platform_game_object[0] = Instantiate (prefab, new Vector3 (11.1549f, 0f, 0f), Quaternion.identity) as GameObject;
			platform_game_object[1] = Instantiate (prefab, new Vector3 (18f, 0f, 0f), Quaternion.identity) as GameObject;
			platform_game_object[2] = Instantiate (prefab, new Vector3 (22.5f, 0f, 0f), Quaternion.identity) as GameObject;
			platform_game_object[4] = Instantiate (prefab, new Vector3 (30.2f, 0f, 0f), Quaternion.identity) as GameObject;
			platform_game_object[5] = Instantiate (prefab, new Vector3 (34.2f, 0f, 0f), Quaternion.identity) as GameObject;
			platform_game_object[6] = Instantiate (prefab, new Vector3 (40.2f, 0f, 0f), Quaternion.identity) as GameObject;

			StartCoroutine("Tutorial1");
			break;
		}
	}

	// Update is called once per frame
	void Update() {
		switch (Application.loadedLevel) {
		case 1:									//Menu
			if (Application.platform == RuntimePlatform.Android){
				if (Input.GetKey(KeyCode.Escape)) {
					if (info_visible) {
						info_panel.SetActive (false);
						info_visible = false;
					}
					 if (signin_visible) {
						signin_panel.SetActive(false);
						signin_visible = false;
					}
				}
			}
			break;
		case 2:									//Gameplay
			Score_update ();
			Fade_points ();
			if (Application.platform == RuntimePlatform.Android){
				if (Input.GetKey(KeyCode.Escape)) {
					Application.LoadLevel("Main_Menu");
				}
			}
			break;
		case 3:									//Tutorial
			if (Application.platform == RuntimePlatform.Android){
				if (Input.GetKey(KeyCode.Escape)) {
					Application.LoadLevel("Main_Menu");
				}
			}
			break;
		}
	}

	IEnumerator DisplaySplashFade() {
		yield return new WaitForSeconds (1f);
		
		for (byte f=255; f >0; f-=15) {
			splash_go.GetComponent<Image> ().color = new Color32(254,254,254,f);
			yield return new WaitForSeconds(0.05f);
		}
		Application.LoadLevel("Main_Menu");
	}
	
	// Called by Update
	void Score_update() {
		//pulls ID of the last platform that pingu hit
		new_platform = pingu_go.GetComponent<pingu_move> ().platform_hit;
		
		//if a new platform was hit then adjust score appropriately
		int difference = new_platform - last_platform;
		
		if (difference <= 1) {			// +1 point for only one platform jump
			score += difference;
		} else {						// +2 points for every platform in a multiple jump
			score += 2 * difference;
			points_go.GetComponent<Text>().text = "+" + (2*difference).ToString ();
			color_alpha = 255f;
		}
		last_platform = new_platform;
		
		//display score to user
		score_go.GetComponent<Text> ().text = score.ToString ();
	} 
	
	void Fade_points() {
		if (color_alpha > 0f) {
			color_alpha -= 10f;
			points_go.GetComponent<Text> ().color = new Color (255f, 251f, 152f, color_alpha);
		} 
	}

	// ===================================================================================
	// ============================        TUTORIAL        ===============================
	// ===================================================================================
	// Called by the tutorial page button presses (Next,Previous)
	public void SwitchTutorial(int page){
		tutorial_page += page;
		switch (tutorial_page) {			//enables or disables the buttons as needed
		case 0:
			score_go.GetComponent<Text> ().text = "0";
			previous.SetActive(false);
			StopCoroutine("Tutorial2");
			Time.timeScale = 1;
			Application.LoadLevel ("Tutorial");
			break;
		case 1: 
			score_go.GetComponent<Text> ().text = "0";
			previous.SetActive(true);
			StopCoroutine("Tutorial1");
			StopCoroutine("Tutorial3");
			StartCoroutine("Tutorial2");
			break;
		case 2: 
			score_go.GetComponent<Text> ().text = "2";
			next.SetActive(true);
			StopCoroutine("Tutorial2");
			StopCoroutine("Tutorial4");
			StartCoroutine("Tutorial3");
			break;
		case 3: 
			score_go.GetComponent<Text> ().text = "4";
			next.SetActive(false);
			StopCoroutine("Tutorial3");
			StartCoroutine("Tutorial4");
			break;
		}
		tutorial_image.sprite = tutorial_sprite [tutorial_page];
	}
	
	IEnumerator Tutorial1(){
		score_go.GetComponent<Text> ().text = "0";
		yield return new WaitForSeconds (1f);
		Time.timeScale = 0;
	}

	IEnumerator Tutorial2(){
		float start = Time.realtimeSinceStartup;
		pingu_go.GetComponent<Transform> ().position = new Vector3 (7.1949f, 2.5949f, 0f);
		Time.timeScale = 0;
		tap.SetActive (true);
		while (Time.realtimeSinceStartup < start + 0.2f) {
			yield return null;
		}
		Time.timeScale = 1;
		tap.SetActive (false);
		pingu_go.GetComponent<pingu_move> ().Tutorial2helper (0.75f);
		yield return new WaitForSeconds (0.65f);
		score_go.GetComponent<Text> ().text = "1";
		Time.timeScale = 0;
		tap.SetActive (true);
		while (Time.realtimeSinceStartup < start + 2.1f) {
			yield return null;
		}
		tap.SetActive (false);
		Time.timeScale = 1;
		pingu_go.GetComponent<pingu_move> ().Tutorial2helper (1.4f);
	    yield return new WaitForSeconds (1.15f);
		score_go.GetComponent<Text> ().text = "2";
		Time.timeScale = 0;
	}
	IEnumerator Tutorial3(){
		float start = Time.realtimeSinceStartup;
		pingu_go.GetComponent<Transform> ().position = new Vector3 (17.9949f, 2.6f, 0f);
		Time.timeScale = 0;
		tap.SetActive (true);
		while (Time.realtimeSinceStartup < start + 0.2f) {
			yield return null;
		}

		Time.timeScale = 1;
		tap.SetActive (false);
		pingu_go.GetComponent<pingu_move> ().Tutorial2helper (1f);
		yield return new WaitForSeconds (0.6f);
		score_go.GetComponent<Text> ().text = "3";
		tap.SetActive (true);
		while (Time.realtimeSinceStartup < start + 1f) {
			yield return null;
		}
		tap.SetActive (false);
		Time.timeScale = 1;
		pingu_go.GetComponent<pingu_move> ().Tutorial2helper (1.4f);
		while (Time.realtimeSinceStartup < start + 2.25f) {
			if ((Time.realtimeSinceStartup - start)%0.30f < 0.16f) {
				tap.SetActive(true);
			} else {
				tap.SetActive(false);
			}
			yield return null;
		}
		tap.SetActive(false);
		score_go.GetComponent<Text> ().text = "4";
		Time.timeScale = 0;
	}
	IEnumerator Tutorial4(){
		float start = Time.realtimeSinceStartup;
		pingu_go.GetComponent<Transform> ().position = new Vector3 (30.23499f, 2.5949f, 0f);
		Time.timeScale = 0;
		tap.SetActive (true);
		while (Time.realtimeSinceStartup < start + 0.2f) {
			yield return null;
		}
		Time.timeScale = 1;
		tap.SetActive (false);
		score_go.GetComponent<Text> ().text = "5";
		pingu_go.GetComponent<pingu_move> ().Tutorial2helper (0.75f);
		yield return new WaitForSeconds (0.6f);
		tap.SetActive (true);
		while (Time.realtimeSinceStartup < start + 1f) {
			yield return null;
		}
		Time.timeScale = 1;
		tap.SetActive (false);
		pingu_go.GetComponent<pingu_move> ().Tutorial2helper (0.75f);
		yield return new WaitForSeconds (2f);
		Time.timeScale = 0;
	}


	// ===================================================================================
	// ============================        BUTTONS         ===============================
	// ===================================================================================
	// Called on press of the Pause UI button
	public void Paused() {
		if (paused) {
			StartCoroutine("Unpause");
		} else {
			paused = true;
			pause_text_go.GetComponent<Text> ().color = new Color32(206, 169, 69 , 255);
			Time.timeScale = 0;
		}	
	}
	IEnumerator Unpause(){
		float start = Time.realtimeSinceStartup;

		pause_text_go.GetComponent<Text> ().text = "3";
		print (paused);
		while (Time.realtimeSinceStartup < start + 1f) {
			yield return null;
		}
		pause_text_go.GetComponent<Text> ().text = "2";
		while (Time.realtimeSinceStartup < start + 2f) {
			yield return null;
		}
		pause_text_go.GetComponent<Text> ().text = "1";
		while (Time.realtimeSinceStartup < start + 3f) {
			yield return null;
		}
		paused = false;
		pause_text_go.GetComponent<Text> ().color = new Color32(206, 169, 69, 0);
		Time.timeScale = 1;
		pause_text_go.GetComponent<Text>().text = "Paused";
	}

	// Called when the player falls between platforms
	public void LevelEnd() {
		Time.timeScale = 0;

		int highscore = PlayerPrefs.GetInt ("highscore");
		int deathcount = PlayerPrefs.GetInt ("deaths");
		int edgecount = PlayerPrefs.GetInt ("edges");
		int total = PlayerPrefs.GetInt ("total");
		int platforms_spawned = PlayerPrefs.GetInt ("platforms");

		gpgs_m.GetComponent<gpgs_manager> ().ReportScore (score);
		gpgs_m.GetComponent<gpgs_manager> ().AchievementCheck (score, highscore, deathcount + 1, edgecount, total + score, platforms_spawned);

		if (score > highscore) {
			PlayerPrefs.SetInt("highscore", score);
			highscore = score;
		}
		if (total < 100001) {
			PlayerPrefs.SetInt("total", total+score);
		}
		PlayerPrefs.SetInt ("deaths", deathcount + 1);

		//gpgs_m.GetComponent<gpgs_manager> ().ReportScore (score);

		overlay_go.SetActive (true);
		final_score_go = GameObject.Find("Current");
		final_best_go = GameObject.Find("Best");
		
		Time.timeScale = 0;
		
		score_image_go.SetActive (false);
		pause_go.SetActive(false);
		final_best_go.GetComponent<Text> ().text = highscore.ToString ();
		final_score_go.GetComponent<Text> ().text = score.ToString ();

		GameObject.Find ("UnityAds").GetComponent<unityads> ().showAd ();
	}

	// Called when the player selects the Menu UI button from pingu_scene
	public void ReturnToMenu() {
		Application.LoadLevel ("Main_Menu");
	}

	// Called when the player selects the play UI button from Main_Menu scene
	public void StartGame() {
		Application.LoadLevel ("pingu_scene");
	}

	// Called when the player selects the 'How to Play' button from Main_Menu scene
	public void ShowTutorial() {
		Application.LoadLevel ("Tutorial");
	}

	// Called when the player selects the 'Music' button from Main_Menu scene
	public void MuteMusic() {
		if (Convert.ToBoolean(PlayerPrefs.GetInt("music"))){
			music_image.sprite = ui_sprite[0];
			source.Stop();
			PlayerPrefs.SetInt("music", 0);
		} else {
			music_image.sprite = ui_sprite[1];
			source.Play();
			PlayerPrefs.SetInt("music", 1);
		}
	}

	// Called when the player selects the 'Sound' button from Main_Menu scene
	public void MuteSound() {
		if (Convert.ToBoolean(PlayerPrefs.GetInt("fx"))){
			sound_image.sprite = ui_sprite[2];
			sound.GetComponent<Song>().fx = false;
			PlayerPrefs.SetInt("fx", 0);
		} else {
			sound_image.sprite = ui_sprite[3];
			sound.GetComponent<Song>().fx = true;
			PlayerPrefs.SetInt("fx", 1);
		} 
	}

	// Called when the player selects the 'info' button from Main_Menu scene
	public void ShowInfo() {
		if (info_visible) {
			info_panel.SetActive(false);
			info_visible = false;
		} else {
			info_visible = true;
			info_panel.SetActive(true);
		}
	}

	public void SignIn() {
		gpgs_m.GetComponent<gpgs_manager> ().OpenSignIn ();
		//signin_info.GetComponent<Text>().text = "Processing...";
	}

	// Will complete the GPGS login using the given credentials
	public void CompletedSignIn(int status) {
		if (status == 1) {				//Sign in was successful
			signin_button_text.GetComponent<Text> ().text = "Sign out of Google Services";
			PlayerPrefs.SetInt("signedin", 1);
		}
		if (status == 2) {				//Sign out was successful
			signin_button_text.GetComponent<Text> ().text = "Sign in to Google Services";
			signin_info.GetComponent<Text>().text = "Signed out successfully!";
			signin_visible = true;
			signin_panel.SetActive(true);
			PlayerPrefs.SetInt("signedin", 0);
		}
		if (status == 0) {				//Sign in failed
			signin_button_text.GetComponent<Text> ().text = "Sign in to Google Services";
			PlayerPrefs.SetInt ("signedin", 0);
		}
		if (status == 4) {
			signin_panel.SetActive(false);
			signin_visible = false;
		}
	}

	public void Achievements() {
		int result = gpgs_m.GetComponent<gpgs_manager> ().OpenAchievements ();
		if (result == 0) {
			signin_info.GetComponent<Text> ().text = "Please sign in to view achievements.";
			signin_visible = true;
			signin_panel.SetActive (true);
		}
	}

	public void Leaderboard() {
		int result = gpgs_m.GetComponent<gpgs_manager> ().OpenLeaderboard ();
		if (result == 0) {
			signin_info.GetComponent<Text> ().text = "Please sign in to view leaderboard.";
			signin_visible = true;
			signin_panel.SetActive (true);
		}
	}
}
