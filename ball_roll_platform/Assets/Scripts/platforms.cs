﻿using UnityEngine;
using System.Collections;

public class platforms : MonoBehaviour {
	private float background_size;
	public int id;

	void Start () {
		background_size = GameObject.FindGameObjectWithTag ("Background").GetComponent<SpriteRenderer> ().bounds.size.x;
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < (Camera.main.transform.position.x - (background_size*.67f)) ){ 
			if (Application.loadedLevel == 2) {  //doesnt destroy platform during tutorial
				Destroy(gameObject);
			}
		}
	}
}
