﻿using UnityEngine;
using System.Collections;

public class cloud_spawner : MonoBehaviour {
	private Camera cam;
	private float camera_height;
	private float camera_width;

	private float random_height;				//y-value for cloud placement
	private float random_width = 0;				//x-value for cloud placement
	
	private int num_of_clouds;			
	private int which_cloud;
	private float cloud_width;
	public GameObject[] clouds_prefab;
	private GameObject clouds_game_object;

	void Awake(){

	}
	// Use this for initialization
	void Start () {
		cam = Camera.main;
		camera_height = 2f*cam.GetComponent<camera_position> ().height;
		camera_width = 2f*cam.GetComponent<camera_position> ().width;
		num_of_clouds = 0;
		//initializing 3 initial clouds
		float num_of_initial_clouds = 4;
		for (int i = 1; i < num_of_initial_clouds; i++) {
			Spawn_Cloud(true,(float)i);		//second value, (float)i/3 , is used to seperate the cloulds. with 3 initial clouds, 
		}
	}
	
	// Update is called once per frame
	void Update () {
		num_of_clouds = GameObject.FindGameObjectsWithTag ("Clouds").Length-1;
		//Spawn more clouds if less than...
		if (num_of_clouds < 5) {
			Spawn_Cloud(false,0);			//second value '0' only needed for initial clouds
		}
	}

	void Spawn_Cloud (bool initial_spawn, float initial_interval) {

		which_cloud = (int)(Random.Range (0, clouds_prefab.Length));
		random_height = Random.Range (.7f * camera_height, .9f * camera_height);

		if (initial_spawn == true) {		//cloud initialization only
			random_width = Random.Range (0, camera_width*initial_interval);
		} 
		else {								//every cloud after initialization
			random_width = Random.Range (cam.transform.position.x + camera_width, cam.transform.position.x + camera_width*2f);
		}
		clouds_game_object = Instantiate (clouds_prefab[which_cloud], new Vector3 (random_width, random_height, 0), Quaternion.identity) as GameObject;
	}
}
