﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pingu_move : MonoBehaviour {
	private Rigidbody2D pingu_RB;
	private GameObject pingu_GO;
	private GameObject bottom_finder = null;
	private GameObject left_finder;
	private GameObject event_sys_go;
	private Animator pingu_animator;
	public float x_velocity = 3f;					// constant velocity pingu will be travelling at all times
	public float y_velocity_paused;					// used to store y-velocity at the time of game pause
	public float jump_force = 4f;					// force to be applied in the y direction (test)
	public float jump_velocity = 4f;				// velocity to be applied in the y direction when jumping 
	private float pingu_size;
	private float platform_size_y;
	private float touch_time;
	private bool touch_held = false;
	private bool flight = false;					//is flight allowed

	public int platform_hit;						//the ID of the platform that pingu landed on

	private AudioSource flap_source;
	public AudioClip flap_clip;
	private AudioSource land_source;
	public AudioClip land_clip;
	/*
	bool grounded = false;
	public Transform ground_check;
	float ground_radius = 0.2f;
	public LayerMask what_is_ground;
	*/

	// Use this for initialization
	void Awake(){
		pingu_GO = GameObject.FindWithTag("Pingu");
		event_sys_go = GameObject.Find ("EventSystem");
		pingu_RB = pingu_GO.GetComponent<Rigidbody2D> ();
		if (Application.loadedLevel == 2) {
			flap_source = transform.GetChild (1).GetComponent<AudioSource> ();
			land_source = transform.GetChild (2).GetComponent<AudioSource> ();
		}
	}


	void Start () {
		pingu_RB.velocity = new Vector2 (x_velocity, -100f);
		pingu_animator = pingu_GO.GetComponent<Animator> ();  			//ANIMATION

		bottom_finder = GameObject.Find ("bottom_finder");
		left_finder = GameObject.Find ("left_finder");
		platform_size_y = GameObject.FindGameObjectWithTag ("Standing Block").GetComponent<SpriteRenderer> ().bounds.size.y/2f;
		if (Application.loadedLevel == 2) {
			if (GameObject.Find ("Song").GetComponent<Song> ().fx == false) {
				flap_source.mute = true;
				land_source.mute = true;
			}
		}
	}


	// Update is called once per frame
	void Update () {
		switch (Application.loadedLevel) {
		case 2:									//Gameplay
			pingu_RB.velocity = new Vector2 (x_velocity, pingu_RB.velocity.y);
			transform.rotation = Quaternion.identity;							//keep pingu from rotating
	

			if (Input.GetMouseButton (0) == true) {								//On mouse left button down, charge multiplier for y jump
				touch_time = touch_time + Time.deltaTime * 3f;					//get time touch is held	
				touch_held = true;												//flag that allows 
				//print ("tap");
			}

			//Trigger animator transitions	
			pingu_animator.SetBool ("Click", touch_held);						//ANIMATION
			pingu_animator.SetFloat ("JumpTime", touch_time);					//ANIMATION
		
			if (Input.GetMouseButton (0) == false && touch_held == true) {
				touch_time = touch_time + .25f;
				if (touch_time >= 1.4f) {
					touch_time = 1.4f;
				}
				if (flight == true) {
					pingu_RB.velocity = new Vector2 (x_velocity, touch_time * 8f);
					if (Application.loadedLevel == 2) {
						flap_source.PlayOneShot(flap_clip);
					}
				}
				touch_time = 0;
				touch_held = false;
				flight = false;
			}

			if (pingu_GO.transform.position.y <= 0f || Input.GetKey ("c")) {
				if (Time.timeScale != 0) {
					event_sys_go.GetComponent<manager> ().LevelEnd ();
				}
			} 
			break;
		}
	}

	public void OnCollisionEnter2D(Collision2D coll) {
		bottom_finder = GameObject.Find ("bottom_finder");
		left_finder = GameObject.Find ("left_finder");
		if ( (bottom_finder.transform.position.y < platform_size_y)
		    && (left_finder.transform.position.x < coll.gameObject.transform.position.x)) {
			x_velocity = -1f;
			if (left_finder.transform.position.y > platform_size_y) {
				int edges = PlayerPrefs.GetInt("edges");
				if (edges <= 200) {
					PlayerPrefs.SetInt("edges", edges+1);
				}
			}
			if (Application.loadedLevel == 2) {
			coll.gameObject.GetComponent<Collider2D>().enabled = false;
			}
		} else {
			flight = true;
			platform_hit = coll.gameObject.GetComponent<platforms> ().id;
			if (platform_hit > 0 ) { 
				land_source.PlayOneShot(land_clip);
			}
		}
	}

	public void Tutorial2helper(float tap_time){
		//float start = Time.realtimeSinceStartup;
		pingu_animator.Play ("Pingu_Charge_Anim");
		x_velocity = 6f;
		pingu_RB.velocity = new Vector2 (x_velocity, tap_time * 8f);
	}


}
