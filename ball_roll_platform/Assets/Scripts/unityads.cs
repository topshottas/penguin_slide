﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class unityads : MonoBehaviour {

	public int ads = 0;
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (GameObject.Find ("UnityAds"));

		if (Advertisement.isSupported) {
			Advertisement.Initialize ("1014437");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void showAd(){
		ads = PlayerPrefs.GetInt ("ads");
		if (ads > 3) {
			if ((Advertisement.isInitialized) && (Advertisement.IsReady ())) {
				Advertisement.Show ();
				PlayerPrefs.SetInt ("ads", 0);
			}
		} else {
			PlayerPrefs.SetInt ("ads", ads + 1);
		}
	}
}
